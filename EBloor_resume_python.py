# Text Variables
Header = '>>> This resume was generated entirely in Python. See https://bitbucket.org/ebloor1/resume/ for sourcecode. <<<'
Name = 'ERICA BLOOR'
Title = 'Physics Doctoral Candidate'
ContactTitle = 'CONTACT'
Contact = '228-731-7665\nerica.bloor@gmail.com\nTallahassee, FL'#\nlinkedin.com/in/ericabloor'
EduHeader = 'EDUCATION'
EduOneTitle = 'Doctorate of Philosophy in Physics'
EduOneLoc = 'Florida State University (FSU)'
EduOneTime = 'Expected August 2020'
EduOneGPA = 'GPA: 3.426'
EduTwoTitle = "Master of Science in Physics"
EduTwoLoc = 'Florida State University (FSU)'
EduTwoTime = 'May 2015'
EduThreeTitle = "Bachelor of Science in Physics"
EduThreeLoc = 'University of Southern Mississippi (USM)'
EduThreeTime = 'May 2013'
EduThreeDesc = 'Major: Physics \nMinor: Mathematics \nGPA: 3.689'
WorkHeader = 'RESEARCH EXPERIENCE'
WorkOneTitle = 'Graduate Research Assistant'
WorkOneLoc = 'FSU Physics Department, Tallahassee, FL'
WorkOneTime = 'January 2014 - Present  (Full-time)'
WorkOneDesc = '-Developing Fortran and Python code to simulate core-collapse supernovae \n-Deriving CCSN model from appropriate hydrodynamics, gravity, neutrino \nheating, and custom metric to simulate and analyze shockwaves\n-Creating a steppingstone to predict which stars explode '
WorkTwoTitle = 'Undergraduate Research Assistant'
WorkTwoProj = 'Developing Methods to Measure Small Attenuation Coefficients Using Short Distance Radiation Detection'
WorkTwoLoc = 'USM, Hattiesburg, MS'
WorkTwoTime = 'August 2011 - May 2013  (12-15 hours/week)'
WorkTwoDesc = '-Created correction for sodium iodide (NaI) gamma-ray detector to be used \nfor determination of small mass-attenuation coefficients with short distance \nmeasurements \n-Analyzed and applied appropriate corrections for raw data provided by the \nNaI detector \n-Experimental and analysis processes corrected radiation detection data \nfor long-range measurements\n-Awarded "USM Physics Student of the Year" 2012-2013'
WorkThreeTitle = 'Medical Physics Research Intern'
WorkThreeLoc = 'Oncologics, INC., Laurel, MS'
WorkThreeTime = 'January 2012 - May 2012  (2-3 hours/week)'
WorkThreeDesc = '-Analyzed correct radiation dosage and placement to eradicate cancer cells \n-Studied linear accelerator and CT machines to gain better understanding \nof radiation oncology \n-Shadowed Medical Physicist, Radiation Oncologist, Dosimetrist, and \ntechnicians'
TeachHeader = 'TEACHING EXPERIENCE'
TeachTitle = 'Graduate Teaching Assistant (TA)'
TeachLoc = 'FSU Physics Department, Tallahassee, FL'
TeachTime = 'August 2013 - August 2020  (9 hours/week)'
TeachDescType = 'Lab TA:\nStudio/Recitation TA:'
TeachDesc1 = 'College Physics (with and without Calculus)'
TeachDesc2 = 'College Physics, Physics for Pre-Med Students'
TeachDesc = '-Instructed students in lab techniques and equipment\n-Taught problem-solving, provided feedback, and facilitated discussions\n-Established and fostered a nurturing and constructive environment for \n20-120 students'
#SkillsHeader = 'SKILLS'
SkillsHeader1 = 'COMPUTER SKILLS'
SkillsHeader2 = 'PHYSICS EXPERTISE'
SkillsOneTitle = 'Computer'
SkillsOneDesc1 = 'Python\nFortran 90\nMatlab\nNumpy\nLaTeX\nLinux'#\nMachine Learning'
SkillsOneDesc2 = 'Microsoft Office:\nWord\nExcel\nPowerPoint'
SkillsTwoTitle = 'Physics Expertise'
SkillsTwoDesc = 'Mechanics\nElectronics\nOptics\nRadiation\nHydrodynamics\nThermodynamics\nNuclear Physics\nQuantum Mechanics\nHigh Energy Physics\nElectricity & Magnetism\nAstrophysics & Cosmology'
ProjectsHeader = 'PUBLICATIONS & PRESENTATIONS'
ProjectOneDesc1 = 'Bloor, E., Murphy, J. W. "Validating neutrino-driven convection model with multi-dimensional simulations - with code Cufe."'
ProjectOneDesc2 = 'In draft. Expected submission June 2020'
ProjectTwoDesc1 = 'Bloor, E. "Developing Methods to Measure Small Attenuation Coefficients Using Short Distance Radiation'
ProjectTwoDesc2 = 'Detection" (2013). Honors Thesis. 155. https://aquila.usm.edu/honors_theses/155'
ProjectThreeDesc1 = 'Seminar presentation: "Neutron Star Subwoofer:Exploring the effect of proto-neutron star convection on the CCSN explosion'
ProjectThreeDesc2 = 'mechanism. FSU Astrophysics Department Seminar, Tallahassee, FL. April 2020'
ProjectFourDesc1 = 'Seminar presentation: "Validating neutrino-driven convection model with multi-dimensional simulations."'
ProjectFourDesc2 = 'FSU Astrophysics Department Seminar, Tallahassee, FL. December 2018'
ProjectFiveDesc1 = 'Poster presentation: "Characterizing the most important instabilities in core-collapse supernovae."'
ProjectFiveDesc2 = '660. WE-Heraeus Seminar: Supernovae - From Simulations to Observations and Nucleosynthetic Fingerprints, \nPhysikzentrum, Bad Honnef, Germany. January 2018 '
ProjectSixDesc1 = 'Poster presentation: "Characterizing the most important instabilities in core-collapse supernovae."'
ProjectSixDesc2 = 'Fifty-One Ergs Conference, NC State, Raleigh, NC. June 2015' 


# Setting style for bar graphs
import matplotlib.pyplot as plt
import matplotlib.colors as color

# set font
plt.rcParams['font.family'] = 'sans-serif'
plt.rcParams['font.sans-serif'] = 'STIXGeneral'

fig, ax = plt.subplots(figsize=(8.5, 11))

#colors
seccolor = '#3e7145' #section header color
#boxcolor = '#C0FA8B' #box color
boxcolor = '#b9de9f' #box color
white = '#ffffff'

# Decorative Lines
#ax.axvline(x=.9, ymin=0, ymax=1, color='#007ACC', alpha=0.0, linewidth=50) #'#007ACC'
plt.axvline(x=.99, color=boxcolor, alpha=0.5, linewidth=320) #righthand box
#plt.axvline(x=.99, ymin=0, ymax=.7, color='#000000', alpha=0.5, linewidth=320) #righthand box
plt.axhline(y=.85, xmin=0, xmax=1, color=white, linewidth=12) #horizontal white line between boxes
plt.axhline(y=1, xmin=0, xmax=1, color=white, linewidth=45) #horizontal white line between boxes
plt.axhline(y=0.01, xmin=0, xmax=1, color=white, linewidth=270) #horizontal white line below box

# set background color
ax.set_facecolor('white')

# remove axes
plt.axis('off')

#locations
left = .01
lefttab = .03
right = .67
righttab = .69
xt = .024 #header-title distance
xi = .0155 #title-info distance

xc = .935 #contact
xe = .81 #edu 
xs = .56-xi #skills
xw = .85 #work
xta = .395 #teach
xp = .22 #publications

# add text
plt.annotate(Header, (left+.015,.98), weight='regular', fontsize=9)
plt.annotate(Name, (left,.925), weight='bold', fontsize=19)
plt.annotate(Title, (left,.895), weight='regular', fontsize=16)
plt.annotate(ContactTitle, (right,xc), weight='bold', fontsize=10, color=seccolor)
plt.annotate(Contact, (right,xc-4*xi), weight='regular', fontsize=10)#, color='#ffffff')
###########################
plt.annotate(EduHeader, (right,xe), weight='bold', fontsize=10, color=seccolor)
plt.annotate(EduOneTitle, (right,xe-xt), weight='bold', fontsize=10)
plt.annotate(EduOneLoc, (right,xe-xt-xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(EduOneTime, (right,xe-xt-2*xi), weight='regular', fontsize=9, alpha=.6)
#plt.annotate(EduOneGPA, (right,xe-xt-3*xi), weight='regular', fontsize=9)
plt.annotate(EduTwoTitle, (right,xe-1.7*xt-3*xi), weight='bold', fontsize=10)
plt.annotate(EduTwoLoc, (right,xe-1.7*xt-4*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(EduTwoTime, (right,xe-1.7*xt-5*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(EduThreeTitle, (right,xe-3*xt-5*xi), weight='bold', fontsize=10)
plt.annotate(EduThreeLoc, (right,xe-3*xt-6*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(EduThreeTime, (right,xe-3*xt-7*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(EduThreeDesc, (right,xe-3*xt-10*xi), weight='regular', fontsize=9)
############################
plt.annotate(SkillsHeader1, (right,xs), weight='bold', fontsize=10, color=seccolor)
plt.annotate(SkillsOneDesc1, (right,xs-xt-4.5*xi), weight='bold', fontsize=9)#, alpha=.6)
plt.annotate(SkillsOneDesc2, (right+.135,xs-xt-2.5*xi), weight='bold', fontsize=9)#, alpha=.6)
plt.annotate(SkillsHeader2, (right,xs-2*xt-5*xi), weight='bold', fontsize=10, color=seccolor)
plt.annotate(SkillsTwoDesc, (right,xs-2*xt-16*xi), weight='bold', fontsize=9)#, alpha=.6)
############################
plt.annotate(WorkHeader, (left,xw), weight='bold', fontsize=10, color=seccolor)
plt.annotate(WorkOneTitle, (left,xw-xt), weight='bold', fontsize=10)
plt.annotate(WorkOneLoc, (left,xw-xt-xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(WorkOneTime, (left,xw-xt-2*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(WorkOneDesc, (lefttab,xw-xt-6*xi), weight='regular', fontsize=9)
plt.annotate(WorkTwoTitle, (left,xw-2*xt-6*xi), weight='bold', fontsize=10)
plt.annotate(WorkTwoLoc, (left,xw-2*xt-7*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(WorkTwoTime, (left,xw-2*xt-8*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(WorkTwoDesc, (lefttab,xw-2*xt-16*xi), weight='regular', fontsize=9)
plt.annotate(WorkThreeTitle, (left,xw-3*xt-16*xi), weight='bold', fontsize=10)
plt.annotate(WorkThreeLoc, (left,xw-3*xt-17*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(WorkThreeTime, (left,xw-3*xt-18*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(WorkThreeDesc, (lefttab,xw-3*xt-23*xi), weight='regular', fontsize=9)
############################
plt.annotate(TeachHeader, (left,xta), weight='bold', fontsize=10, color=seccolor)
plt.annotate(TeachTitle, (left,xta-xt), weight='bold', fontsize=10)
plt.annotate(TeachLoc, (left,xta-xt-xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(TeachTime, (left,xta-xt-2*xi), weight='regular', fontsize=9, alpha=.6)
plt.annotate(TeachDescType, (left,xta-xt-4*xi), weight='bold', fontsize=9)
plt.annotate(TeachDesc1, (left+.077,xta-xt-3*xi), weight='regular', fontsize=9)
plt.annotate(TeachDesc2, (left+.185,xta-xt-4*xi), weight='regular', fontsize=9)
plt.annotate(TeachDesc, (lefttab,xta-xt-8*xi), weight='regular', fontsize=9)
############################
plt.annotate(ProjectsHeader, (left,xp), weight='bold', fontsize=10, color=seccolor)
plt.annotate(ProjectOneDesc1, (left,xp-xt), weight='regular', fontsize=9)
plt.annotate(ProjectOneDesc2, (lefttab,xp-xt-xi), weight='regular', fontsize=9)
plt.annotate(ProjectTwoDesc1, (left,xp-xt-2*xi), weight='regular', fontsize=9)
plt.annotate(ProjectTwoDesc2, (lefttab,xp-xt-3*xi), weight='regular', fontsize=9)
plt.annotate(ProjectThreeDesc1, (left,xp-xt-4*xi), weight='regular', fontsize=9)
plt.annotate(ProjectThreeDesc2, (lefttab,xp-xt-5*xi), weight='regular', fontsize=9)
plt.annotate(ProjectFourDesc1, (left,xp-xt-6*xi), weight='regular', fontsize=9)
plt.annotate(ProjectFourDesc2, (lefttab,xp-xt-7*xi), weight='regular', fontsize=9)
plt.annotate(ProjectFiveDesc1, (left,xp-xt-8*xi), weight='regular', fontsize=9)
plt.annotate(ProjectFiveDesc2, (lefttab,xp-xt-10*xi), weight='regular', fontsize=9)
plt.annotate(ProjectSixDesc1, (left,xp-xt-11*xi), weight='regular', fontsize=9)
plt.annotate(ProjectSixDesc2, (lefttab,xp-xt-12*xi), weight='regular', fontsize=9)

#add qr code
#from matplotlib.offsetbox import TextArea, DrawingArea, OffsetImage, AnnotationBbox
#import matplotlib.image as mpimg
#arr_code = mpimg.imread('ebresumecode.png')
#imagebox = OffsetImage(arr_code, zoom=0.5)
#ab = AnnotationBbox(imagebox, (0.84, 0.12))
#ax.add_artist(ab)

plt.savefig('EBloor_resume_python.pdf', dpi=300, bbox_inches='tight')
